#include "Cliente.hpp"
#include "Menu.hpp"
#include <fstream>
Cliente::Cliente(){
    set_cpf("");
    set_name("");
    set_email("");
    set_telephone("");
}
Cliente::Cliente(string cpf, string name, string email, string telephone){
    set_cpf(cpf);
    set_name(name);
    set_email(email);
    set_telephone(telephone);
}
Cliente::~Cliente(){
    cout << "Destrutor Cliente" << endl;
}
void Cliente::set_cpf(string cpf){
    this->cpf = cpf;
}
string Cliente::get_cpf(){
    return cpf;
}
void Cliente::set_name(string name){
    this->name = name;
}
string Cliente::get_name(){
    return name;
}
void Cliente::set_email(string email){
    this->email = email;
}
string Cliente::get_email(){
    return email;
}
void Cliente::set_telephone(string telephone){
    this->telephone = telephone;
}
string Cliente::get_telephone(){
    return telephone;
}

bool Cliente::verificaCliente(string cpf){
    string read;
    vector <string> vetorCliente;
    int i=0;
    fstream file;
    file.open("clientes.txt", ios::in);
    while (getline(file, read)){
        vetorCliente.push_back(read);
        if (vetorCliente[i]==cpf){
            vetorCliente.clear();
            file.close();
            return true;
        }
        i++;
    }
    vetorCliente.clear();
    file.close();
    return false;
}
void Cliente::cadastraCliente(string cpf){
    Cliente c;
    string name, email, telephone, read;
    fstream file;
    vector <string> vetorCliente;  
    
    
    c.set_cpf(cpf);
    cin.ignore();
    cout << "Digite o nome do cliente"<<endl;
    getline(cin,name);
    c.set_name(name);
    
    cout << "Digite o email" << endl;
    getline(cin,email);
    c.set_email(email);

    cout << "Digite o telefone" << endl;
    getline(cin,telephone);
    c.set_telephone(telephone);

    file.open("clientes.txt", ios::app);
    while(getline(file,read)){
        vetorCliente.push_back(read);
    }
    file.close();
    vetorCliente.push_back(cpf);
    vetorCliente.push_back(name);
    vetorCliente.push_back(email);
    vetorCliente.push_back(telephone);

    file.open("clientes.txt",ios::out | ios::app);
    for(auto i=(vetorCliente.begin());i!=(vetorCliente.end());i++){
        file<<*i<<endl;
    }
    vetorCliente.clear();
    file.close();
    

    cout<<"O cliente foi cadastrado com sucesso"<<endl;
    Menu::venda();
}

fstream file;