#include "Produto.hpp"
#include <fstream>

Produto::Produto(){
    this->code = "";
    this->category = "";
    this->price = 0.0;
    this->qnt = 0;
    this->name = "";

}
Produto::~Produto(){
    
}
void Produto::set_code(string code){
    this->code = code;
}
string Produto::get_code(){
    return code;
}
void Produto::set_name(string name){
    this->name = name;
}
string Produto::get_name(){
    return name;
}
void Produto::set_category(string category){
    this->category = category;
}
string Produto::get_category(){
    return category;
}
void Produto::set_qnt(int qnt){
    this->qnt = qnt;
}
int Produto::get_qnt(){
    return qnt;
}
void Produto::set_price(float price){
    this->price = price;
}
float Produto::get_price(){
    return price;
}
void Produto::cadastraProduto(){
    Produto p;
    string code, name, category, read;
    int qnt;
    float price;
    fstream file;

    cout << "-------CADASTRO DE PRODUTOS-------" <<endl;
    cout << "Digite o código do novo produto"<<endl;
    cin >> code;
    p.set_code(code);
    cout << "Digite o nome do novo produto"<<endl;
    cin >> name;
    p.set_name(name);
    cout << "Digite a quantidade do novo produto"<<endl;
    cin >> qnt;
    p.set_qnt(qnt);
    cout << "Digite o preço do novo produto"<<endl;
    cin >> price;
    p.set_qnt(price);
    cout << "Digite a categoria do novo produto"<<endl;
    cin >> category;
    p.set_category(category);

    file.open("produto.txt", ios ::app);
    file << code << endl;
    file << name << endl;
    file << qnt << endl;
    file << price << endl;
    file << category << endl;

    file.close();
    cout << "Produto cadastrado com sucesso" << endl;
}
void Produto::listarProduto(){

    fstream file;
    string read;
    vector <string> produto;
    produto.clear();
    file.open("produto.txt", ios::in);
    while(getline(file,read)){
        produto.push_back(read);
    }
    file.close();
    cout<<"-------Produtos em Estoque-------"<<endl;
    for(auto i=(produto.begin());i<(produto.end());i+=6){
        cout<<"Codigo: "<<*i<<endl;
        cout<<"Nome: "<<*(i+1)<<endl;
        cout<<"Quantidade: "<<*(i+2)<<endl;
        cout<<"Preco: R$"<<*(i+3)<<endl;
        cout<<"Categoria: "<<*(i+4)<<endl;
        cout << "---------"<<endl;
    }
    int opc;
    cout << "[1]Voltar [0]Finalizar programa." << endl;
    cin >> opc;

    switch (opc)
    {
    case 1: 
        Menu::estoque();
        system("clear");
    case 2:
        exit(0);
    }

}


