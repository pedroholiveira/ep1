#ifndef MENU_HPP
#define MENU_HPP
#include "Cliente.hpp"
#include "Produto.hpp"
#include <string>
#include <cctype>
#include <vector>
#include <iostream>
#include <fstream>

using namespace std;

class Menu{
    public:
        static void modo();
        static void venda();
        static void cadastraCliente(string cpf);
        static bool verificaCliente(string cpf);
        static void estoque();
        static bool cadastraProduto();
        static void recomendacao();
};  

#endif