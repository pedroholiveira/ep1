#ifndef CLIENTE_HPP
#define CLIENTE_HPP

#include <string>
#include <iostream>
#include <vector>

using namespace std;

class Cliente {

    protected:
        string cpf;
        string name;
        string email;
        string telephone;
    public:
        Cliente();
        Cliente(string cpf, string name, string email, string telephone);
        ~Cliente();
        void set_cpf(string cpf);
        string get_cpf();
        void set_name(string name);
        string get_name();
        void set_email(string email);
        string get_email();
        void set_telephone(string telephone);
        string get_telephone();
        static bool verificaCliente(string cpf);      
        static void cadastraCliente(string cpf);
};

#endif