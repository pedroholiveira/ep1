#ifndef PRODUTO_HPP
#define PRODUTO_HPP

#include "Menu.hpp"
#include <iostream>
#include <string>
#include <vector>

using namespace std;

class Produto{
    private:
        string code;
        int qnt;
        string name;
        float price;
        string category;
    public:
        Produto();
        ~Produto();
        void set_code(string code);
        string get_code();
        void set_qnt(int qnt);
        int get_qnt();
        void set_name (string name);
        string get_name();
        void set_price(float price);
        float get_price();
        void set_category(string category);
        string get_category();
        static void cadastraProduto();
        static void listarProduto();
};

#endif